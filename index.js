import yaml from "yaml";
import { readFile } from "fs/promises";
import { resolve } from "path";


const { parse } = yaml;

const cwd = process.cwd();

export const parseYml = async (file) => {
  const filePath = resolve(cwd, "./med-h5.yaml");
  // const filePath = resolve('.', '../fixtures/job.yaml')
  console.log(filePath);
  const fileStr = await readFile(filePath, "utf-8");
  const data = parse(fileStr);
  console.log(data, JSON.stringify(data));
  return data;
};

parseYml()
